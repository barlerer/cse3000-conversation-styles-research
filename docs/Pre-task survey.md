# Pre-task survey

These are the statments which were present in the pre-survey task to estimate the user's prefered conversation style.
The user was asked to rank them from 1 to 5.

1. I love going to the beach 🏝🏖, but honestly I hate running there as is always sand in my shoes!!!
2. I enjoy the beach, but running there is quite irritating because of the sand
3. I swear, these Computer Science 💻 lecture are difficult and long! but they are interesting
4. While the lectures can have increased difficulty than the norm, they do contain interesting aspects
