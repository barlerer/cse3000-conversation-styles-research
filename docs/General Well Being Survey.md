# Well being survey

This were the questions that assembled the survey that the participants have filled. Square brackets show that the text inside served as keyboard buttons in Telegram.

1. What is your energy level today? mark it as a percent from 0 to 100
2. Rate your physical health at the present time. 0 is 'Terrible' and 10 is 'Excellent'
3. Rate your mental health at the present time. 0 is 'Terrible' and 10 is 'Excellent'
4. Looking back over the past month, select the item you think represent you the most. ["Overall, I feel good about my exercise levels", "Overall, I feel good about my sleep quality",
   "Overall, I feel good about my diet", "None of the above", "All of the above"]
5. Looking back over the past month, select the item you think represent you the most. ["I often worry too much about different things", "I often feel like my stress levels are unsustainable",
   "I often feel too tired to complete my activities effectively", "None of the above"]
6. Looking back over the past month, select the item you think represent you the most ["I have enough time and space for myself", "I am engaged and interested in my daily activities", "I am generally optimistic about the future",
   "I am satisfied with my work/life balance", "I am planning to take off time over the end-of-year break", "None of the above"]
7. Looking back over the past month, select the item you think represent you the most ["I have sufficient social interactions with people", "I often feel lonely",
   "I often feel disconnected from my family", "I often feel like I don't have anyone to talk to", "None of the above"]
8. Taking all things together, how satisfied or dissatisfied are you with your life as a whole these days? Where 0 is 'Very dissatisfied' and 10 is 'Very satisfied'
9. Rate the effect of your daily activities on your overall wellbeing, where 0 is 'Very negative effect' and 10 is 'Very positive effect'
10. Which activities or experiences most help your wellbeing?
11. Which activities or experiences most hurt your wellbeing?
12. Rate your current financial situation ["Excellent", "Very Good", "Good",
    "Neutral", "Poor", "Very Poor", "Terrible"]
13. Almost done! We want to better understand your wellbeing needs. Please list out any other specific problems that you have faced in the past month
