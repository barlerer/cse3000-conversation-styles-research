\section{Discussion}
The experiment contained a small sample size which prevents any significant statistical findings. The results have no support for \textbf{Hypothesis \ref{hypothesis1}} and produced near-identical engagement scores with or without style alignment, both in categorical and total scores. These results are likely to be related to the repetitive feedback provided to users in the informal style and to the lack of feedback in the formal style. which might be prejudicial to the natural conversation aspect, as it would resemble a conversation with a robot saying the same text in reply, as opposed to a conversation with a human. In the informal conversation style, a reply was chosen randomly from a list of three possible replies after each incoming message, without any consideration to the user's message. The formal environment provided no feedback to the user's message.

The same cannot be said about satisfaction, which results show to support \textbf{Hypothesis \ref{hypothesis2}}, as the satisfaction was higher for 36\% of the participants. The most interesting aspect of Figure \ref{fig:satisfaction_style_alignment_vs_no} is the density of higher satisfaction ratings in the style alignment environment.

Interestingly, a large majority of participants have preferred the formal conversation style, this result is somewhat counterintuitive. This finding is contrary to previous studies which have suggested to incorporate informal conversation elements in the CA
\cite{kirakowski2009establishing,10.1007/978-3-030-39540-7_4}.
Our results align with previous research on the appropriate conversation style usage in a CA \cite{GRETRY201777,liebrecht2020too}. As the experiment participants had no previous familiarity with the Dandelion bot, it could serve as the reason for the large percentage of participants who preferred formal style. These findings highlight the importance of adaptive chatbots, which would allow for dynamic style alignment, as users get more familiar with a specific chatbot, and after a certain number of interactions might prefer informal style.

\subsection{Verbal Feedback}
We have collected text feedback from users after the evaluation, to analyze further insights that could benefit the implications for future designers of CAs. It is worth mentioning that during the study, the participants were not made aware of the current conversation style or which one they received first.

Some users reported the lack of feedback from the formal conversation to be rude, and that could have influenced the engagement reported by the users.
\begin{displayquote}
	``The conversation felt robotic. This made the conversation feel like I was talking to a bot which doesn't care about what I think.''
\end{displayquote}


A few users mentioned that they have enjoyed the feedback provided by the informal bot. If the user's answers were accepted and matched the required answer format, the bot responded from pre-written replies.
 
\begin{displayquote}
	``I really liked the messages sent by the bot. I knew my messages were accepted and it felt more like a conversation than a survey.''
\end{displayquote}

There were also some comments regarding the rapid response time of the bot. Since the bot responded with no delay between messages, some users missed the feedback provided for their answers.
\begin{displayquote}
	``The bot sent messages so fast, only in middle of survey 1 I saw he replied to my messages.''
\end{displayquote}
This finding is consistent with that of 
\citeauthor{fasterisnotalwaysbetter} who studied the effect of intentional and dynamic delay in conversational agents \cite{fasterisnotalwaysbetter}. They found that delay in bot response lead to better satisfaction rates, as the conversation possessed more human traits and resembled a human-to-human conversation, rather than an automatic, scripted conversation. However, the effects of delay were tested in the context of customer service. There might be a different outcome if the expectation of the users is for an instant reply from the CA. The effects of conversation delays in different contexts are beyond the scope of this paper.


\subsection{Limitations}
\label{limitations}
This study suggests a method to estimate to user's preferred conversation style, by asking a pre-task survey, containing four statements. Grave efforts were made to keep each pair of statements identical in meaning, but dissimilar in the style of writing. No pretest for reliability was performed in this study due to time constraints. The possibility of other attributes rather than style influencing the ranking cannot be ruled out.

A technical constraint was posed by the Dandelion system. In its original implementation, Dandelion did not support Neuro-linguistic programming (NLP). Therefor, when Dandelion received answers to questions asked during a survey, it had no technical ability to gather an intent and understand what the answer is about. Such constraint limited the ability to respond with an appropriate response to the user's answers. We used pre-written responses which were picked at random with equal probability.

Due to budget constraints, relatively small sample size was selected for this research (N=30). As with any experiment which revolved unpredictable variables, a larger sample size could mitigate any effects caused by outliers.

\subsection{Design Implications}
Our study found that using a dynamically aligned conversation style matched to the user's preference can have benefits such as an increase in satisfaction levels. While our evidence does not show that conversation style alignment can increase engagement, we believe these results might have been influenced by other CA design elements that were discussed in Section \ref{limitations}.
By adopting a conversation style that matches the user's preferred style, CA creators may experience recurrent interaction from the users \cite{liebrecht2020too}.

If engagement marks an important role in a CA design, creators need to contemplate on other contributing factors, such as intended message delay \cite{fasterisnotalwaysbetter} and integrating NLP to gather intent and provide proper conversational social cues \cite{doi:10.1080/07421222.2020.1790204}. Given these factors, more research is needed combined with conversational style alignment to measure the potential difference in engagement.


\subsection{Future Work}
\subsubsection{Implicit Detection of Preferred Style}
This research suggests a method to identify the user's preferred conversation style through a pre-task survey. While it does have the benefit of potentially identifying the user's preferred conversation style, it does require more time from the user. Another possible option is to perform the pre-task not before every task, but perhaps, every other task, as a user's preferred conversation style might not change that often. We would also like to mention the possibility of implicitly detecting the user's preferred conversation style, without explicitly asking for it. 

An exploratory study experimented with different conversation styles in the context of conversational agents \cite{shamekhi2017an}. Their results reside with the theory concept laid by Tannen \cite{tannen_2005}, where users would prefer talking with another entity that has a similar conversation style as their own. These results open up the possibility of analyzing the user's previously stored answers to best try to estimate the preferred style. This approach, while being transparent from the user, does require an extensive amount of saved users' responses, which might not always be available. More research is needed on implicitly detecting the user's preferred conversation style.

\subsubsection{Context Influenced Conversation Style}
The conversation style preferred by the user can also be influenced by the context of the conversation \cite{10.1145/3392837}. A survey asking a user to rate and provide feedback on a university course might foster different emotions to a survey asking the user about his personal health records, or personal well-being. More research is needed, to better understand the specific desires of a user's preferred conversation style regarding different conversation topics or domains.